{
'name': 'Dokumen_Kapal',
'version': '1.0',
'summary': 'Module for Odoo Dokumen Kapal',
'category': 'Extra Tools',
'author': 'Ilyas',
'maintainer':'Ilyas',
'website': 'akfitally.akfigroup.com',
'license': 'AGPL-3',
'depends': [
'base',
'mail'
],
'data': [
'dokumen_view.xml',
'ir.model.access.csv'
],
'installable': True,
'application': True,
'sequence': '1',
'auto_install': False,
}
