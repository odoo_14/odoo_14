from odoo import models, fields
class DokumenKapal(models.Model):
      _name ='dokumen.kapal'
      _description= 'Dokumen Kapal'
      _inherit = ['mail.thread', 'mail.activity.mixin']  
      
      nama = fields.Char('Nama Kapal', required=True)
      katagori = fields.Char('Katagori', tracking=True)
      tahun_terbit = fields.Char('Tahun Terbit', tracking=True)
      masa_berlaku = fields.Char('Masa Berlaku', tracking=True)
      status = fields.Char('Status', tracking=True)
      trans_in = fields.Date('Transaksi Masuk', tracking=True)
      trans_out = fields.Date('Transaksi Keluar', tracking=True)
      jumlah = fields.Integer('Jumlah', tracking=True)
      satuan = fields.Char('Satuan', tracking=True)
      posisi_kapal = fields.Char('Posisi Kapal', tracking=True)
      lokasi_bongkar_muat = fields.Char('Lokasi Bongkar Muat', tracking=True)
      lokasi_saat_ini = fields.Char('Lokasi Saat Ini', tracking=True)      
      kapten = fields.Char('Nama Kapten', tracking=True) 
      file_pendukung = fields.Binary('File Pendukung', tracking=True)
